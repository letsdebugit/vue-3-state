import Header from './components/header/header.js'
import routes from './views/index.js'
const { createApp } = Vue
const { createRouter, createWebHashHistory } = VueRouter

const history = createWebHashHistory()
const router = createRouter({ routes, history })

const App = createApp({
  components: {
    'app-header': Header
  },

  mounted () {
    console.log('READY.')
  }
})

window.addEventListener('load', () => {
  App.use(router)
  App.mount('main')
})
