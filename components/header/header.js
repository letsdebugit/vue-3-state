const template = `
<header>
  <nav>
    <router-link to="/">Home</router-link>
    <router-link to="/users">Users</router-link>
    <router-link to="/about">About</router-link>
  </nav>

  <div class="login" v-if="state.isLoggedIn">
    Welcome, {{ state.userName }}
    <button @click="logout">Log out</button>
  </div>

  <div class="login" v-else>
    User: <input v-model="userName">
    Password: <input v-model="password" type="password">
    <button @click="login(userName, password)">Log in</button>
  </div>
</header>
`

import sessionStore from '../../store/session-store.js'

export default {
  template,

  setup () {
    let userName = ''
    let password = ''

    // Get state and mutations from session store.
    // State is a reactive object, so it can be directly
    // used in data bindings.
    // If you'd wish to extract some properties from state
    // into local variables, use `sessionStore.extract`
    // to retain reactivity. This is demonstrated in details
    // in `views/home.js`
    const { state, login, logout } = sessionStore

    return {
      state,
      userName,
      password,
      login,
      logout
    }
  }
}
