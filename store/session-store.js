import { createStore } from './vue-state.js'

export default createStore({
  userName: 'tomasz',
  isLoggedIn: true,

  async login (userName, password) {
    await this.logout()
    if (userName && password) {
      this.isLoggedIn = true
      this.userName = userName
    }
  },

  async logout () {
    this.isLoggedIn = false
    this.userName = null
  },

  async getUsers () {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    if (response.ok) {
      const users = await response.json()
      return users
    }
  }
})
