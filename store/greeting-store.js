import { createStore } from './vue-state.js'

export default createStore({
  greeting: 'Click me ...',
  counter: 0,

  confirm (greeting) {
    this.greeting = greeting
    this.counter++
  }
})
