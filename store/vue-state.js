const { reactive, readonly, computed } = Vue

/**
 * Creates a state store
 * @param store Store options, containing
 *    state           initial state
 *    ...mutations    methods mutating the state, accessible via `this` inside the method body
 * @description
 * The method returns state store containing a reactive and readonly `state` property,
 * along with mutation methods for safe operations on the state.
 */
export function createStore (store = {}) {
  // Create reactive state
  const entries = Object.entries(store)
  const state = reactive(entries
    .filter(entry => typeof entry[1] !== 'function')
    .reduce((all, [name, value]) => {
      all[name] = value
      return all
    }, {})
  )

  // Create mutation functions with the state available through `this`
  const mutations = entries
    .filter(entry => typeof entry[1] === 'function')
    .reduce((all, [name, handler]) => {
      all[name] = (...args) => {
        return handler.call(state, ...args)
      }
      return all
    }, {})

  // Add mutations to state object.
  // Since state becomes `this` context inside mutations,
  // now any mutation can call another mutation.
  // This unfortunately was not possible in Vuex,
  // which sometimes resulted in code duplication or need
  // to extract mutation logic to local functions.
  for (const [name, mutation] of Object.entries(mutations)) {
    state[name] = mutation
  }

  // Return readonly state, so that the outside world
  // is not able to mutate the state directly.
  const safeState = readonly(state)

  return {
    /**
     * State mutations
     */
    ...mutations,

    /**
     * Readonly state.
     * Outside world will not be able to mutate the state directly.
     */
    get state () {
      return safeState
    },

    /**
     * Map function for extracting state values as reactive properties,
     * providing similar functionality as Vuex mapState function.
     * @param values Values to extract from state
     * @example
     * Allowed syntax:
     *    const { value } = extract('value')
     *    const { value } = extract(value => state.value)
     *    const { value1, value2 } = extract('value1', 'value2')
     *    const { value1, value2 } = extract(['value1', 'value2'])
     *    const { value } = extract({
     *      value1 => state.value1,
     *      value2 => state.value2
     *    })
     * @description
     * In code and data bindings you could use fully qualified paths
     * to state properties, for example state.greeting or state.counter.
     *
     * If you'd extract them from state by simple destructuring, they would
     * lose reactivity though, similar to Vue 3 props which behave the same.
     * If you need such extracted state properties to be reactive,
     *
     * representing the specified state properties or expressions.
     *
     * Warning! As it's customary with Vue 3, computed expressions containing
     * primitive-type values are technically refs, so you need to use `.value`
     * to get into their actual values.
     */
    extract (...values) {
      let expressions
      let names
      let items = []

      // If array > 1 element, it must be state property names
      if (values.length > 1) {
        names = values
      } else if (values.length === 1) {
        // If one element, check its type, whether property name, array of names or otherwise a dictionary of expressions
        const value = values[0]
        if (typeof value === 'string') names = [value]
        else if (Array.isArray(value)) names = value
        else expressions = value
      } else {
        return {}
      }

      if (names) {
        // Simple list of property names
        items = names
          .map(name =>
            ({ name, value: computed(() => safeState[name]) }))
      } else if (expressions) {
        // Dictionary of property names and expressions extracting values from state
        items = Object
          .entries(expressions)
          .map(([name, expression]) =>
            ({ name, value: computed(() => expression(safeState)) }))
      }

      // Return an object with computed properties
      const result = items
        .reduce((all, { name, value }) => {
          all[name] = value
          return all
        }, {})

      return result
    }
  }
}
