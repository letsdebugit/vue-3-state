const template = `
  <main class="users">
    <h1>Users</h1>

    <ul v-if="isLoggedIn">
      <li v-for="user in users" :key="user.id">
        {{ user.name }}, {{ user.email }}
      </li>
    </ul>
  </main>
`
import sessionStore from '../../store/session-store.js'

export default {
  template,

  data () {
    return {
      users: []
    }
  },

  computed: {
    isLoggedIn () {
      return sessionStore.state.isLoggedIn
    }
  },

  async created () {
    this.users = await sessionStore.getUsers()
  }
}
