import Home from './home/home.js'
import Users from './users/users.js'
import About from './about/about.js'

export default [
  {
    name: 'home',
    path: '/',
    component: Home
  },
  {
    name: 'users',
    path: '/users',
    component: Users
  },
  {
    name: 'about',
    path: '/about',
    component: About
  }
]
