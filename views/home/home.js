const { computed } = Vue

const template = `
  <main class="home">
    <h1>Home</h1>

    <div v-if="isLoggedIn">
      <h2>
        Welcome, {{ userName }}
      </h2>
      <div @click="confimed" :style="{ color: hintColor }">
        {{ greeting }} {{ hint }}
      </div>
    </div>

    <div v-else>
      To use the application, you must first log in.
    </div>
  </main>
`

import greetingStore from '../../store/greeting-store.js'
import sessionStore from '../../store/session-store.js'

export default {
  template,

  setup () {
    // Get `isLoggedIn` and `userName` reactive properties from session store
    const { isLoggedIn, userName } = sessionStore.extract('isLoggedIn', 'userName')
    // Get `confirm` action from greetingStore
    const { confirm } = greetingStore
    // Get `counter` and `greeting` reactive properties from greeting store
    const { counter, greeting } = greetingStore.extract('counter', 'greeting')
    // Get a computed expression based on a property from greeting state
    const { counterTimesThree } = greetingStore.extract({ counterTimesThree: state => state.counter * 3 })

    // Create some computed variables based on state properties
    const hint = computed(() =>
      counter.value === 0 ? '' : `(# of clicks: ${counter.value}. And ${counter.value} x 3 is ${counterTimesThree.value})`)
    const hintColor = computed(() =>
      counter.value === 0 ? 'orangered' : 'green')

    // A function performing state mutation
    function confimed () {
      if (counter.value === 0) {
        confirm('Thank you. You can try again!')
      } else {
        confirm('Okay, okay, enough!')
      }

      // This must not be allowed, so it will fail with error: `target is readonly`
      greetingStore.state.greeting = 'abc'
    }

    return {
      hint,
      hintColor,
      counter,
      counterTimesThree,
      greeting,
      isLoggedIn,
      userName,

      confimed
    }
  }
}
