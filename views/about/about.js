const template = `
  <main class="about">
    <h1>About</h1>
    <div v-if="isLoggedIn">
    Welcome, {{ userName }}!
    </div>
    <div>
    A simple proposal for managing state in Vue 3 applications
    </div>
    <div>
    (c) Tomasz Waraksa, Dublin, 2020
    </div>
  </main>
`
import sessionStore from '../../store/session-store.js'

export default {
  template,

  setup () {
    const { userName, isLoggedIn } = sessionStore.extract('userName', 'isLoggedIn')

    return {
      userName,
      isLoggedIn
    }
  }
}
